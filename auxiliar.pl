tira1([E|L], [E], L).

tira1([E1|L], N, [E1|L1]):- tira1(L,N,L1).

permutar([],[]).

permutar(L,[X|L1]):- member1(X,L,L0), permutar(L0,L1).

member1(X,[X|L],L).

member1(X,[A|L],[A|L1]):- member1(X,L,L1).

t(A,L):- permutar(L,R), append(A,_B,R).
